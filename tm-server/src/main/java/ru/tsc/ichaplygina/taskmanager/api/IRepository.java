package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractModelDTO;

import java.util.List;

public interface IRepository<E extends AbstractModelDTO> {

    void add(@NotNull E entity);

    void addAll(@Nullable List<E> entities);

    void clear();

    @NotNull
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    int getSize();

    boolean isEmpty();

    void remove(@NotNull E entity);

    int removeById(@NotNull String id);

}
