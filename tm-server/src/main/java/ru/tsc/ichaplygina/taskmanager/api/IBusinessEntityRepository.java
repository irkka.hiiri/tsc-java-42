package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractBusinessEntityDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityRepository<E extends AbstractBusinessEntityDTO> extends IRepository<E> {

    void add(@NotNull String userId, @NotNull String name, @Nullable String description);

    void clearForUser(@NotNull String currentUserId);

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator);

    @NotNull
    List<E> findAllForUser(@NotNull String userId);

    @NotNull
    List<E> findAllForUser(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    E findByIndex(int index);

    @Nullable
    E findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    E findByName(@NotNull String name);

    @Nullable
    E findByNameForUser(@NotNull String userId, @NotNull String name);

    @NotNull
    List<String> findKeysForUser(@NotNull String userId);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, String name);

    int getSizeForUser(@NotNull String userId);

    boolean isEmptyForUser(@NotNull String userId);

    boolean isNotFoundById(@NotNull String id);

    boolean isNotFoundByIdForUser(@NotNull String userId, @NotNull String id);

    int removeByIdForUser(@NotNull String userId, @NotNull String id);

    int removeByIndex(int index);

    int removeByIndexForUser(@NotNull String userId, int index);

    int removeByName(@NotNull String name);

    int removeByNameForUser(@NotNull String userId, @NotNull String name);

    int update(@NotNull String id, @NotNull String name, @Nullable String description);

    int updateForUser(@NotNull String userId, @NotNull String id, @NotNull String name, @Nullable String description);

    @Nullable
    E updateStatusById(@NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIdForUser(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    E updateStatusByIndex(int index, @NotNull Status status);

    @Nullable
    E updateStatusByIndexForUser(@NotNull String userId, int index, @NotNull Status status);

    @Nullable
    E updateStatusByName(@NotNull String name, @NotNull Status status);

    @Nullable
    E updateStatusByNameForUser(@NotNull String userId, @NotNull String name, @NotNull Status status);

}
