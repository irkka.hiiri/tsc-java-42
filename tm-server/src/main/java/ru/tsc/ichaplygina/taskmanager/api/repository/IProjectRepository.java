package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.handler.StatusTypeHandler;

import java.sql.Timestamp;
import java.util.List;

public interface IProjectRepository extends IBusinessEntityRepository<ProjectDTO> {

    @Insert("INSERT INTO tm_project (ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID)" +
            " VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{started}, #{completed}, #{userId})")
    void add(@Param("id") @NotNull String id, @Param("name") @NotNull String name, @Param("description") @Nullable String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("started") @NotNull Timestamp started, @Param("completed") @NotNull Timestamp completed,
             @Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE USER_ID = #{userId}")
    void clearForUser(@Param("userId") @NotNull String userid);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE ID = #{id}")
    int completeProjectById(@Param("id") String id, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE ID = #{id} AND USER_ID = #{userId}")
    int completeProjectByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                                   @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} " +
            "WHERE ID = (SELECT ID FROM tm_project ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int completeProjectByIndex(@Param("index") int index, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} " +
            "WHERE ID = (SELECT ID FROM tm_project WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int completeProjectByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index,
                                      @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp completed);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE NAME = #{name}")
    int completeProjectByName(@Param("name") String name, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_project SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE NAME = #{name} AND USER_ID = #{userId}")
    int completeProjectByNameForUser(@Param("userId") @NotNull String userId, @Param("name") String name,
                                     @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp completed);

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID FROM tm_project")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    List<ProjectDTO> findAllForUser(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    ProjectDTO findById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    ProjectDTO findByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    ProjectDTO findByIndex(@Param("index") int index);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE user_id = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID")
    })
    ProjectDTO findByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE name = #{name} LIMIT 1")
    @Result(property = "id", column = "ID")
    @Result(property = "name", column = "NAME")
    @Result(property = "description", column = "DESCRIPTION")
    @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class)
    @Result(property = "created", column = "CREATED_DT")
    @Result(property = "dateStart", column = "STARTED_DT")
    @Result(property = "dateFinish", column = "COMPLETED_DT")
    @Result(property = "userId", column = "USER_ID")
    ProjectDTO findByName(@Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID " +
            "FROM tm_project WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    @Result(property = "id", column = "ID")
    @Result(property = "name", column = "NAME")
    @Result(property = "description", column = "DESCRIPTION")
    @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class)
    @Result(property = "created", column = "CREATED_DT")
    @Result(property = "dateStart", column = "STARTED_DT")
    @Result(property = "dateFinish", column = "COMPLETED_DT")
    @Result(property = "userId", column = "USER_ID")
    ProjectDTO findByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT id FROM tm_project ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    String getIdByIndex(@Param("index") int index);

    @Nullable
    @Select("SELECT id FROM tm_project WHERE user_id = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    String getIdByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Nullable
    @Select("SELECT id FROM tm_project WHERE name = #{name}")
    String getIdByName(@Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT id FROM tm_project WHERE name = #{name} AND user_id = #{userId}")
    String getIdByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Select("SELECT COUNT(1) FROM tm_project")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getSizeForUser(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_project WHERE ID = #{id}")
    int removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_project WHERE ID = #{id} AND USER_ID = #{userId}")
    int removeByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_project WHERE ID = (SELECT ID FROM tm_project ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int removeByIndex(@Param("index") int index);

    @Delete("DELETE FROM tm_project WHERE ID = (SELECT ID FROM tm_project WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int removeByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Delete("DELETE FROM tm_project WHERE NAME = #{name}")
    int removeByName(@Param("name") @NotNull String name);

    @Delete("DELETE FROM tm_project WHERE NAME = #{name} AND USER_ID = #{userId}")
    int removeByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} WHERE ID = #{id}")
    int startProjectById(@Param("id") @NotNull String id, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} WHERE ID = #{id} AND USER_ID = #{userId}")
    int startProjectByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                                @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} " +
            "WHERE ID = (SELECT ID FROM tm_project ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int startProjectByIndex(@Param("index") int index, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} " +
            "WHERE ID = (SELECT ID FROM tm_project WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int startProjectByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index,
                                   @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} WHERE NAME = #{name}")
    int startProjectByName(@Param("name") String name, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET STATUS = #{status}, STARTED_DT = #{started} WHERE NAME = #{name} AND USER_ID = #{userId}")
    int startProjectByNameForUser(@Param("userId") @NotNull String userId, @Param("name") String name,
                                  @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_project SET NAME = #{name}, DESCRIPTION = #{description} WHERE ID = #{id}")
    int update(@Param("id") @NotNull String id, @Param("name") @NotNull String name, @Param("description") @Nullable String description);

    @Update("UPDATE tm_project SET NAME = #{name}, DESCRIPTION = #{description} WHERE ID = #{id} AND USER_ID = #{userId}")
    int updateForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                      @Param("name") @NotNull String name, @Param("description") @Nullable String description);

}
