package ru.tsc.ichaplygina.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.entity.IWBS;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntityDTO extends AbstractModelDTO implements IWBS {

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "completed_dt")
    private Date dateFinish;

    @Nullable
    @Column(name = "started_dt")
    private Date dateStart;

    @Nullable
    @Column
    private String description;

    @NotNull
    @Column
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Column(name = "user_id")
    private String userId;

    public AbstractBusinessEntityDTO(@NotNull final String name, @NotNull final String userId) {
        this.name = name;
        this.userId = userId;
    }

    public AbstractBusinessEntityDTO(@NotNull final String name, @Nullable final String description, @NotNull final String userId) {
        this.name = name;
        this.description = description != null ? description : EMPTY;
        this.userId = userId;
    }

    @Override
    public String toString() {
        @NotNull final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return getId()
                + DELIMITER
                + (!isEmptyString(name) ? name : PLACEHOLDER)
                + DELIMITER
                + (!isEmptyString(description) ? description : PLACEHOLDER)
                + DELIMITER
                + formatter.format(created)
                + DELIMITER
                + status
                + DELIMITER
                + (dateStart == null ? PLACEHOLDER : formatter.format(dateStart))
                + DELIMITER
                + (dateFinish == null ? PLACEHOLDER : formatter.format(dateFinish))
                + DELIMITER
                + getUserId();
    }

}
