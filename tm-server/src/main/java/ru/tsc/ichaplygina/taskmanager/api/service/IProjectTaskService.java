package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;

import java.util.List;

public interface IProjectTaskService {

    @Nullable TaskDTO addTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void clearProjects(@NotNull String userId);

    @NotNull List<TaskDTO> findAllTasksByProjectId(@NotNull String userId,
                                                   @NotNull String projectId,
                                                   @NotNull String sortBy);

    @Nullable ProjectDTO removeProjectById(@NotNull String userId, @Nullable String projectId);

    @Nullable ProjectDTO removeProjectByIndex(@NotNull String userId, int index);

    @Nullable ProjectDTO removeProjectByName(@NotNull String userId, @NotNull String projectName);

    @Nullable TaskDTO removeTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);
}
