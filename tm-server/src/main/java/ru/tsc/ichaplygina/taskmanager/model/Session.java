package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

import static java.lang.System.currentTimeMillis;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public class Session extends AbstractModel {

    @Nullable
    @Column
    private String signature;

    @Column(name = "time_stamp")
    private long timeStamp = currentTimeMillis();

    @NotNull
    @ManyToOne
    private User user;

}
