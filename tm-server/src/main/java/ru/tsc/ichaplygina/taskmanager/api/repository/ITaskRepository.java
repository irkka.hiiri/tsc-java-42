package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.handler.StatusTypeHandler;

import java.sql.Timestamp;
import java.util.List;

public interface ITaskRepository extends IBusinessEntityRepository<TaskDTO> {

    @Insert("INSERT INTO tm_task (ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID)" +
            " VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{started}, #{completed}, #{userId}, #{projectId})")
    void add(@Param("id") @NotNull String id, @Param("name") @NotNull String name, @Param("description") @Nullable String description,
             @Param("status") @NotNull String status, @Param("created") @NotNull Timestamp created,
             @Param("started") @NotNull Timestamp started, @Param("completed") @NotNull Timestamp completed,
             @Param("userId") @NotNull String userId, @Param("projectId") @Nullable String projectId);

    @Update("UPDATE tm_task SET PROJECT_ID = #{projectId} WHERE ID = #{taskId}")
    @Nullable int addTaskToProject(@Param("taskId") @NotNull String taskId, @Param("projectId") @NotNull String projectId);

    @Update("UPDATE tm_task SET PROJECT_ID = #{projectId} WHERE ID = #{taskId} AND USER_ID = #{userId}")
    @Nullable int addTaskToProjectForUser(@Param("userId") @NotNull String userId,
                                          @Param("taskId") @NotNull String taskId,
                                          @Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE USER_ID = #{userId}")
    void clearForUser(@Param("userId") @NotNull String userid);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE ID = #{id}")
    int completeTaskById(@Param("id") String id, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE ID = #{id} AND USER_ID = #{userId}")
    int completeTaskByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                                @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} " +
            "WHERE ID = (SELECT ID FROM tm_task ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int completeTaskByIndex(@Param("index") int index, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} " +
            "WHERE ID = (SELECT ID FROM tm_task WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int completeTaskByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index,
                                   @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE NAME = #{name}")
    int completeTaskByName(@Param("name") String name, @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @Update("UPDATE tm_task SET STATUS = #{status}, COMPLETED_DT = #{completed} WHERE NAME = #{name} AND USER_ID = #{userId}")
    int completeTaskByNameForUser(@Param("userId") @NotNull String userId, @Param("name") String name,
                                  @Param("status") @NotNull String status, @Param("completed") @Nullable Timestamp completed);

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID FROM tm_task")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    List<TaskDTO> findAll();

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID FROM tm_task WHERE project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    List<TaskDTO> findAllByProjectId(@Param("projectId") @NotNull String projectId);

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE PROJECT_ID = #{projectId} AND USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    List<TaskDTO> findAllByProjectIdForUser(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @NotNull
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID FROM tm_task WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    List<TaskDTO> findAllForUser(@Param("userId") @NotNull String userId);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findByIndex(@Param("index") int index);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE user_id = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE name = #{name}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findByName(@Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE name = #{name} AND user_id = #{userId} ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE id = #{taskId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findTaskInProject(@Param("taskId") @NotNull String taskId, @Param("projectId") @NotNull String projectId);

    @Nullable
    @Select("SELECT ID, NAME, DESCRIPTION, STATUS, CREATED_DT, STARTED_DT, COMPLETED_DT, USER_ID, PROJECT_ID " +
            "FROM tm_task WHERE id = #{taskId} AND project_id = #{projectId} AND user_id = #{userId} ")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "name", column = "NAME"),
            @Result(property = "description", column = "DESCRIPTION"),
            @Result(property = "status", column = "STATUS", typeHandler = StatusTypeHandler.class),
            @Result(property = "created", column = "CREATED_DT"),
            @Result(property = "dateStart", column = "STARTED_DT"),
            @Result(property = "dateFinish", column = "COMPLETED_DT"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "projectId", column = "PROJECT_ID"),
    })
    TaskDTO findTaskInProjectForUser(@Param("userId") @NotNull String userId,
                                     @Param("taskId") @NotNull String taskId,
                                     @Param("projectId") @NotNull String projectId);

    @Nullable
    @Select("SELECT id FROM tm_task ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    String getIdByIndex(@Param("index") int index);

    @Nullable
    @Select("SELECT id FROM tm_task WHERE user_id = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index}")
    String getIdByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Nullable
    @Select("SELECT id FROM tm_task WHERE name = #{name}")
    String getIdByName(@Param("name") @NotNull String name);

    @Nullable
    @Select("SELECT id FROM tm_task WHERE name = #{name} AND user_id = #{userId}")
    String getIdByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Select("SELECT COUNT(1) FROM tm_task")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId}")
    int getSizeForUser(@Param("userId") @NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE PROJECT_ID = #{projectId}")
    void removeAllByProjectId(@Param("projectId") @NotNull String projectId);

    @Delete("DELETE FROM tm_task WHERE ID = #{id}")
    int removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_task WHERE ID = #{id} AND USER_ID = #{userId}")
    int removeByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_task WHERE ID = (SELECT ID FROM tm_task ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int removeByIndex(@Param("index") int index);

    @Delete("DELETE FROM tm_task WHERE ID = (SELECT ID FROM tm_task " +
            "WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int removeByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index);

    @Delete("DELETE FROM tm_task WHERE NAME = #{name}")
    int removeByName(@Param("name") @NotNull String name);

    @Delete("DELETE FROM tm_task WHERE NAME = #{name} AND USER_ID = #{userId}")
    int removeByNameForUser(@Param("userId") @NotNull String userId, @Param("name") @NotNull String name);

    @Update("UPDATE tm_task SET PROJECT_ID = null WHERE ID = #{taskId} AND PROJECT_ID= #{projectId}")
    void removeTaskFromProject(@Param("taskId") @NotNull String taskId, @Param("projectId") @NotNull String projectId);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} WHERE ID = #{id}")
    int startTaskById(@Param("id") @NotNull String id, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} WHERE ID = #{id} AND USER_ID = #{userId}")
    int startTaskByIdForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                             @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} " +
            "WHERE ID = (SELECT ID FROM tm_task ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int startTaskByIndex(@Param("index") int index, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} " +
            "WHERE ID = (SELECT ID FROM tm_task WHERE USER_ID = #{userId} ORDER BY CREATED_DT LIMIT 1 OFFSET #{index})")
    int startTaskByIndexForUser(@Param("userId") @NotNull String userId, @Param("index") int index,
                                @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} WHERE NAME = #{name}")
    int startTaskByName(@Param("name") String name, @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET STATUS = #{status}, STARTED_DT = #{started} WHERE NAME = #{name} AND USER_ID = #{userId}")
    int startTaskByNameForUser(@Param("userId") @NotNull String userId, @Param("name") String name,
                               @Param("status") @NotNull String status, @Param("started") @Nullable Timestamp started);

    @Update("UPDATE tm_task SET NAME = #{name}, DESCRIPTION = #{description} WHERE ID = #{id}")
    int update(@Param("id") @NotNull String id, @Param("name") @NotNull String name, @Param("description") @Nullable String description);

    @Update("UPDATE tm_task SET NAME = #{name}, DESCRIPTION = #{description} WHERE ID = #{id} AND USER_ID = #{userId}")
    int updateForUser(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id,
                      @Param("name") @NotNull String name, @Param("description") @Nullable String description);


}
