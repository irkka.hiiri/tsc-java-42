package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService(name = "TaskEndpoint")
public final class TaskEndpoint {

    @NotNull
    private final IProjectTaskService projectTaskService;
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ITaskService taskService,
                        @NotNull final IProjectTaskService projectTaskService,
                        @NotNull final ISessionService sessionService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void addTaskToProject(@WebParam(name = "session") @Nullable final SessionDTO session,
                                 @WebParam(name = "projectId") @NotNull final String projectId,
                                 @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(projectTaskService.addTaskToProject(session.getUserId(), projectId, taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void clearTasks(@WebParam(name = "session") @Nullable final SessionDTO session) {
        sessionService.validateSession(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public void completeTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                 @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeById(session.getUserId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void completeTaskByIndex(@WebParam(name = "session") @Nullable final SessionDTO session,
                                    @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeByIndex(session.getUserId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void completeTaskByName(@WebParam(name = "session") @Nullable final SessionDTO session,
                                   @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.completeByName(session.getUserId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void createTask(@WebParam(name = "session") @Nullable final SessionDTO session,
                           @WebParam(name = "taskName") @NotNull final String taskName,
                           @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        taskService.add(session.getUserId(), taskName, taskDescription);
    }

    @WebMethod
    public TaskDTO findTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                                @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        return taskService.findById(session.getUserId(), taskId);
    }

    @WebMethod
    public TaskDTO findTaskByIndex(@WebParam(name = "session") @Nullable final SessionDTO session,
                                   @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        return taskService.findByIndex(session.getUserId(), taskIndex);
    }

    @WebMethod
    public TaskDTO findTaskByName(@WebParam(name = "session") @Nullable final SessionDTO session,
                                  @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        return taskService.findByName(session.getUserId(), taskName);
    }

    @WebMethod
    public List<TaskDTO> getTaskList(@WebParam(name = "session") @Nullable final SessionDTO session,
                                     @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return taskService.findAll(session.getUserId(), sortBy);
    }

    @WebMethod
    public List<TaskDTO> getTaskListByProject(@WebParam(name = "session") @Nullable final SessionDTO session,
                                              @WebParam(name = "projectId") @NotNull final String projectId,
                                              @WebParam(name = "sortBy") @Nullable final String sortBy) {
        sessionService.validateSession(session);
        return projectTaskService.findAllTasksByProjectId(session.getUserId(), projectId, sortBy);
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                               @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeById(session.getUserId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskByIndex(@WebParam(name = "session") @Nullable final SessionDTO session,
                                  @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeByIndex(session.getUserId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskByName(@WebParam(name = "session") @Nullable final SessionDTO session,
                                 @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.removeByName(session.getUserId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void removeTaskFromProject(@WebParam(name = "session") @Nullable final SessionDTO session,
                                      @WebParam(name = "projectId") @NotNull final String projectId,
                                      @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        projectTaskService.removeTaskFromProject(session.getUserId(), projectId, taskId);
    }

    @WebMethod
    public void startTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                              @WebParam(name = "taskId") @NotNull final String taskId) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startById(session.getUserId(), taskId))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void startTaskByIndex(@WebParam(name = "session") @Nullable final SessionDTO session,
                                 @WebParam(name = "taskIndex") final int taskIndex) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startByIndex(session.getUserId(), taskIndex))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void startTaskByName(@WebParam(name = "session") @Nullable final SessionDTO session,
                                @WebParam(name = "taskName") @NotNull final String taskName) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.startByName(session.getUserId(), taskName))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void updateTaskById(@WebParam(name = "session") @Nullable final SessionDTO session,
                               @WebParam(name = "taskId") @NotNull final String taskId,
                               @WebParam(name = "taskName") @NotNull final String taskName,
                               @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.updateById(session.getUserId(), taskId, taskName, taskDescription))
                .orElseThrow(TaskNotFoundException::new);
    }

    @WebMethod
    public void updateTaskByIndex(@WebParam(name = "session") @Nullable final SessionDTO session,
                                  @WebParam(name = "taskIndex") final int taskIndex,
                                  @WebParam(name = "taskName") @NotNull final String taskName,
                                  @WebParam(name = "taskDescription") @Nullable final String taskDescription) {
        sessionService.validateSession(session);
        Optional.ofNullable(taskService.updateByIndex(session.getUserId(), taskIndex, taskName, taskDescription))
                .orElseThrow(TaskNotFoundException::new);
    }

}
