package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.property.ISessionProperty;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public final class SignatureUtil {

    @Nullable
    public static String md5(@NotNull final String value) {
        if (isEmptyString(value)) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (@NotNull java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    public static SessionDTO sign(@NotNull final SessionDTO session, @NotNull final ISessionProperty sessionProperty) {
        session.setSignature(null);
        final int iteration = sessionProperty.getSignIteration();
        @NotNull final String secret = sessionProperty.getSignSecret();
        @Nullable String result = session.toString();
        for (int i = 0; i < iteration; i++) result = md5(secret + result + secret);
        session.setSignature(result);
        return session;
    }

}
