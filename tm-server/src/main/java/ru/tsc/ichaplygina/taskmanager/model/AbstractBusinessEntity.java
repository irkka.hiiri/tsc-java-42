package ru.tsc.ichaplygina.taskmanager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractModel {

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "completed_dt")
    private Date dateFinish;

    @Nullable
    @Column(name = "started_dt")
    private Date dateStart;

    @Nullable
    @Column
    private String description;

    @NotNull
    @Column
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @ManyToOne
    private User user;

}
