package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class TaskService extends AbstractBusinessEntityService<TaskDTO> implements ITaskService {

    @NotNull
    private final IUserService userService;

    public TaskService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService, userService);
        this.userService = userService;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final TaskDTO task) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String id = task.getId();
        @NotNull final String name = task.getName();
        @Nullable final String description = task.getDescription();
        @NotNull final String status = task.getStatus().toString();
        @NotNull final Timestamp created = new Timestamp(task.getCreated().getTime());
        @Nullable final Timestamp started = task.getDateStart() == null ? null : new Timestamp(task.getDateStart().getTime());
        @Nullable final Timestamp completed = task.getDateFinish() == null ? null : new Timestamp(task.getDateFinish().getTime());
        @NotNull final String userId = task.getUserId();
        @Nullable final String projectId = task.getProjectId();
        try {
            taskRepository.add(id, name, description, status, created, started, completed, userId, projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO(name, description, userId);
        add(task);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskDTO> taskList) {
        if (taskList == null) return;
        for (final TaskDTO task : taskList) add(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public final TaskDTO addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.addTaskToProject(taskId, projectId) : taskRepository.addTaskToProjectForUser(userId, taskId, projectId);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return taskRepository.findById(taskId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            if (userService.isPrivilegedUser(userId)) taskRepository.clear();
            else taskRepository.clearForUser(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO completeById(@NotNull final String userId, @NotNull final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskById(taskId, status, timestamp) :
                    taskRepository.completeTaskByIdForUser(userId, taskId, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(userId, taskId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO completeByIndex(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskByIndex(index, status, timestamp) :
                    taskRepository.completeTaskByIndexForUser(userId, index, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskByName(name, status, timestamp) :
                    taskRepository.completeTaskByNameForUser(userId, name, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByName(userId, name);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findAll() : taskRepository.findAllForUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                    taskRepository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public final List<TaskDTO> findAllByProjectId(@NotNull final String userId,
                                                  @NotNull final String projectId,
                                                  @Nullable final String sortBy) {
        @NotNull final Comparator<TaskDTO> comparator = getComparator(sortBy);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findAllByProjectId(projectId).stream().sorted(comparator).collect(Collectors.toList()) :
                    taskRepository.findAllByProjectIdForUser(userId, projectId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findById(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findById(taskId) : taskRepository.findByIdForUser(userId, taskId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findByIndex(entityIndex) : taskRepository.findByIndexForUser(userId, entityIndex);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.findByName(entityName) : taskRepository.findByNameForUser(userId, entityName);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.getIdByIndex(index) : taskRepository.getIdByIndexForUser(userId, index);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.getIdByName(entityName) : taskRepository.getIdByNameForUser(userId, entityName);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return taskRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    taskRepository.getSize() : taskRepository.getSizeForUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@NotNull final String projectId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllByProjectId(projectId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = findById(id);
            final int affectedRows = taskRepository.removeById(id);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = findById(userId, id);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.removeById(id) :
                    taskRepository.removeByIdForUser(userId, id);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = findByIndex(userId, index);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.removeByIndex(index) :
                    taskRepository.removeByIndexForUser(userId, index);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = findByName(userId, name);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.removeByName(name) :
                    taskRepository.removeByNameForUser(userId, name);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final TaskDTO removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = userService.isPrivilegedUser(userId) ?
                    taskRepository.findTaskInProject(taskId, projectId) : taskRepository.findTaskInProjectForUser(userId, taskId, projectId);
            Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
            taskRepository.removeTaskFromProject(taskId, projectId);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO startById(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskById(taskId, status, timestamp) :
                    taskRepository.completeTaskByIdForUser(userId, taskId, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(userId, taskId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO startByIndex(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskByIndex(index, status, timestamp) :
                    taskRepository.completeTaskByIndexForUser(userId, index, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.completeTaskByName(name, status, timestamp) :
                    taskRepository.completeTaskByNameForUser(userId, name, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByName(userId, name);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateById(@NotNull final String userId,
                              @NotNull final String taskId,
                              @NotNull final String taskName,
                              @Nullable final String taskDescription) {
        if (isEmptyString(taskId)) throw new IdEmptyException();
        if (isEmptyString(taskName)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    taskRepository.update(taskId, taskName, taskDescription) :
                    taskRepository.updateForUser(userId, taskId, taskName, taskDescription);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(taskId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO updateByIndex(@NotNull final String userId,
                                 final int entityIndex,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }

}
