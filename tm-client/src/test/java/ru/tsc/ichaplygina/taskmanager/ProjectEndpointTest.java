package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;
import ru.tsc.ichaplygina.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class ProjectEndpointTest {

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private SessionDTO session;

    @Before
    public void initTest() {
        session = sessionEndpoint.openSession("root", "toor");
        projectEndpoint.createProject(session, "project 1", "");
        projectEndpoint.createProject(session, "project 2", "");
    }

    @Test
    @Category(SoapCategory.class)
    public void testClearProjects() {
        projectEndpoint.clearProjects(session);
        Assert.assertEquals(0, projectEndpoint.getProjectList(session, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectById() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        projectEndpoint.completeProjectById(session, projectId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByIdBadId() {
        @NotNull final String projectId = "???";
        projectEndpoint.completeProjectById(session, projectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectByIndex() {
        projectEndpoint.completeProjectByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByIndexBadIndex() {
        projectEndpoint.completeProjectByIndex(session, 2);
    }

    @Test
    @Category(SoapCategory.class)
    public void testCompleteProjectByName() {
        projectEndpoint.completeProjectByName(session, "project 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testCompleteProjectByNameBadName() {
        projectEndpoint.completeProjectByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testCreateProject() {
        projectEndpoint.createProject(session, "project 3", "desc 1");
        Assert.assertEquals(3, projectEndpoint.getProjectList(session, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetProjectList() {
        Assert.assertEquals(2, projectEndpoint.getProjectList(session, "").size());
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindProjectById() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        Assert.assertNotNull(projectEndpoint.findProjectById(session, projectId));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindProjectByIdBadId() {
        @NotNull final String projectId = "...";
        Assert.assertNull(projectEndpoint.findProjectById(session, projectId));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindProjectByIndex() {
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(session, 0));
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testFindProjectByIndexBadIndex() {
        projectEndpoint.findProjectByIndex(session, 2);
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindProjectByName() {
        Assert.assertNotNull(projectEndpoint.findProjectByName(session, "project 1"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindProjectByNameBadName() {
        Assert.assertNull(projectEndpoint.findProjectByName(session, "unknown name"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectById() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        projectEndpoint.removeProjectById(session, projectId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByIdBadId() {
        @NotNull final String projectId = "???";
        projectEndpoint.removeProjectById(session, projectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndex() {
        projectEndpoint.removeProjectByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByIndexBadIndex() {
        projectEndpoint.removeProjectByIndex(session, 2);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveProjectByName() {
        projectEndpoint.removeProjectByName(session, "project 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveProjectByNameBadName() {
        projectEndpoint.removeProjectByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectById() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        projectEndpoint.startProjectById(session, projectId);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByIdBadId() {
        @NotNull final String projectId = "???";
        projectEndpoint.startProjectById(session, projectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectByIndex() {
        projectEndpoint.startProjectByIndex(session, 0);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByIndexBadIndex() {
        projectEndpoint.startProjectByIndex(session, 2);
    }

    @Test
    @Category(SoapCategory.class)
    public void testStartProjectByName() {
        projectEndpoint.startProjectByName(session, "project 1");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testStartProjectByNameBadName() {
        projectEndpoint.startProjectByName(session, "Bad Name");
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectById() {
        @NotNull final String projectId = projectEndpoint.findProjectByName(session, "project 1").getId();
        projectEndpoint.updateProjectById(session, projectId, "project 1", "new description");
        @NotNull final ProjectDTO project = projectEndpoint.findProjectByName(session, "project 1");
        Assert.assertEquals("new description", project.getDescription());

    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIdBadId() {
        @NotNull final String projectId = "...";
        projectEndpoint.updateProjectById(session, projectId, "project 1", "new description");
        projectEndpoint.findProjectByName(session, "project 1");

    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndex() {
        projectEndpoint.updateProjectByIndex(session, 0, "project 1", "new description");
        @NotNull final ProjectDTO project = projectEndpoint.findProjectByName(session, "project 1");
        Assert.assertEquals("new description", project.getDescription());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProjectByIndexBadIndex() {
        projectEndpoint.updateProjectByIndex(session, 2, "project 1", "new description");
    }

    @After
    public void closeTest() {
        projectEndpoint.clearProjects(session);
        adminEndpoint.saveBackup(session);
        sessionEndpoint.closeSession(session);
    }

}
