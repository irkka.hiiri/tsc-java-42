package ru.tsc.ichaplygina.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;
import ru.tsc.ichaplygina.taskmanager.marker.SoapCategory;

import javax.xml.ws.WebServiceException;

public class AdminEndpointTest {

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private SessionDTO session;

    @Before
    public void initTest() {
        session = sessionEndpoint.openSession("root", "toor");
        adminEndpoint.addUser(session, "user", "user", "user@user", "USER", "", "", "");
    }

    @Test
    @Category(SoapCategory.class)
    public void testAddUser() {
        adminEndpoint.addUser(session, "test", "1", "1", "USER", "1", "1", "1");
        Assert.assertNotNull(sessionEndpoint.openSession("test", "1"));
        adminEndpoint.removeUserByLogin(session, "test");
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void testAddUserAccessDenied() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("user", "user");
        adminEndpoint.addUser(session, "test1", "1", "1", "USER", "1", "1", "1");
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangePassword() {
        adminEndpoint.changePassword(session, "user", "123");
        Assert.assertNotNull(sessionEndpoint.openSession("user", "123"));
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void testChangePasswordAccessDenied() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("user", "user");
        adminEndpoint.changePassword(session, "root", "123");
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangePasswordUser() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("user", "user");
        adminEndpoint.changePassword(session, "user", "123");
        sessionEndpoint.openSession("user", "123");
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangeRole() {
        adminEndpoint.addUser(session, "test", "test", "test@test", "USER", "", "", "");
        adminEndpoint.changeRole(session, "user", "ADMIN");
        @NotNull final SessionDTO newSession = sessionEndpoint.openSession("user", "user");
        adminEndpoint.changeRole(newSession, "test", "ADMIN");
        adminEndpoint.removeUserByLogin(session, "test");
        adminEndpoint.changeRole(session, "user", "USER");
    }

    @Test
    @Category(SoapCategory.class)
    public void testFindUserByLogin() {
        Assert.assertNotNull(adminEndpoint.findUserByLogin(session, "user"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testGetUsers() {
        Assert.assertTrue(adminEndpoint.getUsers(session).size() > 0);
    }

    @Category(SoapCategory.class)
    @Test(expected = WebServiceException.class)
    public void testGetUsersAccessDenied() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("user", "user");
        adminEndpoint.getUsers(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void testLockUserByLogin() {
        adminEndpoint.lockUserByLogin(session, "user");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLockUserByLoginAndTryToLogIn() {
        adminEndpoint.lockUserByLogin(session, "user");
        Assert.assertNotNull(sessionEndpoint.openSession("user", "user"));
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveUserByLogin() {
        adminEndpoint.removeUserByLogin(session, "user");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveUserByLoginAndTryToLogIn() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("root", "toor");
        adminEndpoint.removeUserByLogin(session, "user");
        sessionEndpoint.openSession("user", "user");
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnlockUserByLoginAlreadyUnlocked() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("root", "toor");
        adminEndpoint.unlockUserByLogin(session, "user");
    }

    @Test
    @Category(SoapCategory.class)
    public void testLockAndUnlockUserByLogin() {
        @NotNull final SessionDTO session = sessionEndpoint.openSession("root", "toor");
        adminEndpoint.lockUserByLogin(session, "user");
        adminEndpoint.unlockUserByLogin(session, "user");
    }

    @After
    public void closeTest() {
        if (adminEndpoint.findUserByLogin(session, "user") != null)
            adminEndpoint.removeUserByLogin(session, "user");
        adminEndpoint.saveBackup(session);
        sessionEndpoint.closeSession(session);
    }

}
