
package ru.tsc.ichaplygina.taskmanager.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tsc.ichaplygina.taskmanager.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ClearProjects_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "clearProjects");
    private final static QName _ClearProjectsResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "clearProjectsResponse");
    private final static QName _CompleteProjectById_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectById");
    private final static QName _CompleteProjectByIdResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectByIdResponse");
    private final static QName _CompleteProjectByIndex_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectByIndex");
    private final static QName _CompleteProjectByIndexResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectByIndexResponse");
    private final static QName _CompleteProjectByName_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectByName");
    private final static QName _CompleteProjectByNameResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "completeProjectByNameResponse");
    private final static QName _CreateProject_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "createProject");
    private final static QName _CreateProjectResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "createProjectResponse");
    private final static QName _FindProjectById_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectById");
    private final static QName _FindProjectByIdResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectByIdResponse");
    private final static QName _FindProjectByIndex_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectByIndex");
    private final static QName _FindProjectByIndexResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectByIndexResponse");
    private final static QName _FindProjectByName_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectByName");
    private final static QName _FindProjectByNameResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "findProjectByNameResponse");
    private final static QName _GetProjectList_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "getProjectList");
    private final static QName _GetProjectListResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "getProjectListResponse");
    private final static QName _RemoveProjectById_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectById");
    private final static QName _RemoveProjectByIdResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectByIdResponse");
    private final static QName _RemoveProjectByIndex_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectByIndex");
    private final static QName _RemoveProjectByIndexResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectByIndexResponse");
    private final static QName _RemoveProjectByName_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectByName");
    private final static QName _RemoveProjectByNameResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "removeProjectByNameResponse");
    private final static QName _StartProjectById_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectById");
    private final static QName _StartProjectByIdResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectByIdResponse");
    private final static QName _StartProjectByIndex_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectByIndex");
    private final static QName _StartProjectByIndexResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectByIndexResponse");
    private final static QName _StartProjectByName_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectByName");
    private final static QName _StartProjectByNameResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "startProjectByNameResponse");
    private final static QName _UpdateProjectById_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "updateProjectById");
    private final static QName _UpdateProjectByIdResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "updateProjectByIdResponse");
    private final static QName _UpdateProjectByIndex_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "updateProjectByIndex");
    private final static QName _UpdateProjectByIndexResponse_QNAME = new QName("http://endpoint.taskmanager.ichaplygina.tsc.ru/", "updateProjectByIndexResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.ichaplygina.taskmanager.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ClearProjects }
     * 
     */
    public ClearProjects createClearProjects() {
        return new ClearProjects();
    }

    /**
     * Create an instance of {@link ClearProjectsResponse }
     * 
     */
    public ClearProjectsResponse createClearProjectsResponse() {
        return new ClearProjectsResponse();
    }

    /**
     * Create an instance of {@link CompleteProjectById }
     * 
     */
    public CompleteProjectById createCompleteProjectById() {
        return new CompleteProjectById();
    }

    /**
     * Create an instance of {@link CompleteProjectByIdResponse }
     * 
     */
    public CompleteProjectByIdResponse createCompleteProjectByIdResponse() {
        return new CompleteProjectByIdResponse();
    }

    /**
     * Create an instance of {@link CompleteProjectByIndex }
     * 
     */
    public CompleteProjectByIndex createCompleteProjectByIndex() {
        return new CompleteProjectByIndex();
    }

    /**
     * Create an instance of {@link CompleteProjectByIndexResponse }
     * 
     */
    public CompleteProjectByIndexResponse createCompleteProjectByIndexResponse() {
        return new CompleteProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link CompleteProjectByName }
     * 
     */
    public CompleteProjectByName createCompleteProjectByName() {
        return new CompleteProjectByName();
    }

    /**
     * Create an instance of {@link CompleteProjectByNameResponse }
     * 
     */
    public CompleteProjectByNameResponse createCompleteProjectByNameResponse() {
        return new CompleteProjectByNameResponse();
    }

    /**
     * Create an instance of {@link CreateProject }
     * 
     */
    public CreateProject createCreateProject() {
        return new CreateProject();
    }

    /**
     * Create an instance of {@link CreateProjectResponse }
     * 
     */
    public CreateProjectResponse createCreateProjectResponse() {
        return new CreateProjectResponse();
    }

    /**
     * Create an instance of {@link FindProjectById }
     * 
     */
    public FindProjectById createFindProjectById() {
        return new FindProjectById();
    }

    /**
     * Create an instance of {@link FindProjectByIdResponse }
     * 
     */
    public FindProjectByIdResponse createFindProjectByIdResponse() {
        return new FindProjectByIdResponse();
    }

    /**
     * Create an instance of {@link FindProjectByIndex }
     * 
     */
    public FindProjectByIndex createFindProjectByIndex() {
        return new FindProjectByIndex();
    }

    /**
     * Create an instance of {@link FindProjectByIndexResponse }
     * 
     */
    public FindProjectByIndexResponse createFindProjectByIndexResponse() {
        return new FindProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link FindProjectByName }
     * 
     */
    public FindProjectByName createFindProjectByName() {
        return new FindProjectByName();
    }

    /**
     * Create an instance of {@link FindProjectByNameResponse }
     * 
     */
    public FindProjectByNameResponse createFindProjectByNameResponse() {
        return new FindProjectByNameResponse();
    }

    /**
     * Create an instance of {@link GetProjectList }
     * 
     */
    public GetProjectList createGetProjectList() {
        return new GetProjectList();
    }

    /**
     * Create an instance of {@link GetProjectListResponse }
     * 
     */
    public GetProjectListResponse createGetProjectListResponse() {
        return new GetProjectListResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectById }
     * 
     */
    public RemoveProjectById createRemoveProjectById() {
        return new RemoveProjectById();
    }

    /**
     * Create an instance of {@link RemoveProjectByIdResponse }
     * 
     */
    public RemoveProjectByIdResponse createRemoveProjectByIdResponse() {
        return new RemoveProjectByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectByIndex }
     * 
     */
    public RemoveProjectByIndex createRemoveProjectByIndex() {
        return new RemoveProjectByIndex();
    }

    /**
     * Create an instance of {@link RemoveProjectByIndexResponse }
     * 
     */
    public RemoveProjectByIndexResponse createRemoveProjectByIndexResponse() {
        return new RemoveProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectByName }
     * 
     */
    public RemoveProjectByName createRemoveProjectByName() {
        return new RemoveProjectByName();
    }

    /**
     * Create an instance of {@link RemoveProjectByNameResponse }
     * 
     */
    public RemoveProjectByNameResponse createRemoveProjectByNameResponse() {
        return new RemoveProjectByNameResponse();
    }

    /**
     * Create an instance of {@link StartProjectById }
     * 
     */
    public StartProjectById createStartProjectById() {
        return new StartProjectById();
    }

    /**
     * Create an instance of {@link StartProjectByIdResponse }
     * 
     */
    public StartProjectByIdResponse createStartProjectByIdResponse() {
        return new StartProjectByIdResponse();
    }

    /**
     * Create an instance of {@link StartProjectByIndex }
     * 
     */
    public StartProjectByIndex createStartProjectByIndex() {
        return new StartProjectByIndex();
    }

    /**
     * Create an instance of {@link StartProjectByIndexResponse }
     * 
     */
    public StartProjectByIndexResponse createStartProjectByIndexResponse() {
        return new StartProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link StartProjectByName }
     * 
     */
    public StartProjectByName createStartProjectByName() {
        return new StartProjectByName();
    }

    /**
     * Create an instance of {@link StartProjectByNameResponse }
     * 
     */
    public StartProjectByNameResponse createStartProjectByNameResponse() {
        return new StartProjectByNameResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectById }
     * 
     */
    public UpdateProjectById createUpdateProjectById() {
        return new UpdateProjectById();
    }

    /**
     * Create an instance of {@link UpdateProjectByIdResponse }
     * 
     */
    public UpdateProjectByIdResponse createUpdateProjectByIdResponse() {
        return new UpdateProjectByIdResponse();
    }

    /**
     * Create an instance of {@link UpdateProjectByIndex }
     * 
     */
    public UpdateProjectByIndex createUpdateProjectByIndex() {
        return new UpdateProjectByIndex();
    }

    /**
     * Create an instance of {@link UpdateProjectByIndexResponse }
     * 
     */
    public UpdateProjectByIndexResponse createUpdateProjectByIndexResponse() {
        return new UpdateProjectByIndexResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProjects }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "clearProjects")
    public JAXBElement<ClearProjects> createClearProjects(ClearProjects value) {
        return new JAXBElement<ClearProjects>(_ClearProjects_QNAME, ClearProjects.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearProjectsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "clearProjectsResponse")
    public JAXBElement<ClearProjectsResponse> createClearProjectsResponse(ClearProjectsResponse value) {
        return new JAXBElement<ClearProjectsResponse>(_ClearProjectsResponse_QNAME, ClearProjectsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectById")
    public JAXBElement<CompleteProjectById> createCompleteProjectById(CompleteProjectById value) {
        return new JAXBElement<CompleteProjectById>(_CompleteProjectById_QNAME, CompleteProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectByIdResponse")
    public JAXBElement<CompleteProjectByIdResponse> createCompleteProjectByIdResponse(CompleteProjectByIdResponse value) {
        return new JAXBElement<CompleteProjectByIdResponse>(_CompleteProjectByIdResponse_QNAME, CompleteProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectByIndex")
    public JAXBElement<CompleteProjectByIndex> createCompleteProjectByIndex(CompleteProjectByIndex value) {
        return new JAXBElement<CompleteProjectByIndex>(_CompleteProjectByIndex_QNAME, CompleteProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectByIndexResponse")
    public JAXBElement<CompleteProjectByIndexResponse> createCompleteProjectByIndexResponse(CompleteProjectByIndexResponse value) {
        return new JAXBElement<CompleteProjectByIndexResponse>(_CompleteProjectByIndexResponse_QNAME, CompleteProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectByName")
    public JAXBElement<CompleteProjectByName> createCompleteProjectByName(CompleteProjectByName value) {
        return new JAXBElement<CompleteProjectByName>(_CompleteProjectByName_QNAME, CompleteProjectByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteProjectByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "completeProjectByNameResponse")
    public JAXBElement<CompleteProjectByNameResponse> createCompleteProjectByNameResponse(CompleteProjectByNameResponse value) {
        return new JAXBElement<CompleteProjectByNameResponse>(_CompleteProjectByNameResponse_QNAME, CompleteProjectByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "createProject")
    public JAXBElement<CreateProject> createCreateProject(CreateProject value) {
        return new JAXBElement<CreateProject>(_CreateProject_QNAME, CreateProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProjectResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "createProjectResponse")
    public JAXBElement<CreateProjectResponse> createCreateProjectResponse(CreateProjectResponse value) {
        return new JAXBElement<CreateProjectResponse>(_CreateProjectResponse_QNAME, CreateProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectById")
    public JAXBElement<FindProjectById> createFindProjectById(FindProjectById value) {
        return new JAXBElement<FindProjectById>(_FindProjectById_QNAME, FindProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectByIdResponse")
    public JAXBElement<FindProjectByIdResponse> createFindProjectByIdResponse(FindProjectByIdResponse value) {
        return new JAXBElement<FindProjectByIdResponse>(_FindProjectByIdResponse_QNAME, FindProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectByIndex")
    public JAXBElement<FindProjectByIndex> createFindProjectByIndex(FindProjectByIndex value) {
        return new JAXBElement<FindProjectByIndex>(_FindProjectByIndex_QNAME, FindProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectByIndexResponse")
    public JAXBElement<FindProjectByIndexResponse> createFindProjectByIndexResponse(FindProjectByIndexResponse value) {
        return new JAXBElement<FindProjectByIndexResponse>(_FindProjectByIndexResponse_QNAME, FindProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectByName")
    public JAXBElement<FindProjectByName> createFindProjectByName(FindProjectByName value) {
        return new JAXBElement<FindProjectByName>(_FindProjectByName_QNAME, FindProjectByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindProjectByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "findProjectByNameResponse")
    public JAXBElement<FindProjectByNameResponse> createFindProjectByNameResponse(FindProjectByNameResponse value) {
        return new JAXBElement<FindProjectByNameResponse>(_FindProjectByNameResponse_QNAME, FindProjectByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "getProjectList")
    public JAXBElement<GetProjectList> createGetProjectList(GetProjectList value) {
        return new JAXBElement<GetProjectList>(_GetProjectList_QNAME, GetProjectList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "getProjectListResponse")
    public JAXBElement<GetProjectListResponse> createGetProjectListResponse(GetProjectListResponse value) {
        return new JAXBElement<GetProjectListResponse>(_GetProjectListResponse_QNAME, GetProjectListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectById")
    public JAXBElement<RemoveProjectById> createRemoveProjectById(RemoveProjectById value) {
        return new JAXBElement<RemoveProjectById>(_RemoveProjectById_QNAME, RemoveProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectByIdResponse")
    public JAXBElement<RemoveProjectByIdResponse> createRemoveProjectByIdResponse(RemoveProjectByIdResponse value) {
        return new JAXBElement<RemoveProjectByIdResponse>(_RemoveProjectByIdResponse_QNAME, RemoveProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectByIndex")
    public JAXBElement<RemoveProjectByIndex> createRemoveProjectByIndex(RemoveProjectByIndex value) {
        return new JAXBElement<RemoveProjectByIndex>(_RemoveProjectByIndex_QNAME, RemoveProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectByIndexResponse")
    public JAXBElement<RemoveProjectByIndexResponse> createRemoveProjectByIndexResponse(RemoveProjectByIndexResponse value) {
        return new JAXBElement<RemoveProjectByIndexResponse>(_RemoveProjectByIndexResponse_QNAME, RemoveProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectByName")
    public JAXBElement<RemoveProjectByName> createRemoveProjectByName(RemoveProjectByName value) {
        return new JAXBElement<RemoveProjectByName>(_RemoveProjectByName_QNAME, RemoveProjectByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "removeProjectByNameResponse")
    public JAXBElement<RemoveProjectByNameResponse> createRemoveProjectByNameResponse(RemoveProjectByNameResponse value) {
        return new JAXBElement<RemoveProjectByNameResponse>(_RemoveProjectByNameResponse_QNAME, RemoveProjectByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectById")
    public JAXBElement<StartProjectById> createStartProjectById(StartProjectById value) {
        return new JAXBElement<StartProjectById>(_StartProjectById_QNAME, StartProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectByIdResponse")
    public JAXBElement<StartProjectByIdResponse> createStartProjectByIdResponse(StartProjectByIdResponse value) {
        return new JAXBElement<StartProjectByIdResponse>(_StartProjectByIdResponse_QNAME, StartProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectByIndex")
    public JAXBElement<StartProjectByIndex> createStartProjectByIndex(StartProjectByIndex value) {
        return new JAXBElement<StartProjectByIndex>(_StartProjectByIndex_QNAME, StartProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectByIndexResponse")
    public JAXBElement<StartProjectByIndexResponse> createStartProjectByIndexResponse(StartProjectByIndexResponse value) {
        return new JAXBElement<StartProjectByIndexResponse>(_StartProjectByIndexResponse_QNAME, StartProjectByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectByName")
    public JAXBElement<StartProjectByName> createStartProjectByName(StartProjectByName value) {
        return new JAXBElement<StartProjectByName>(_StartProjectByName_QNAME, StartProjectByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartProjectByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "startProjectByNameResponse")
    public JAXBElement<StartProjectByNameResponse> createStartProjectByNameResponse(StartProjectByNameResponse value) {
        return new JAXBElement<StartProjectByNameResponse>(_StartProjectByNameResponse_QNAME, StartProjectByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "updateProjectById")
    public JAXBElement<UpdateProjectById> createUpdateProjectById(UpdateProjectById value) {
        return new JAXBElement<UpdateProjectById>(_UpdateProjectById_QNAME, UpdateProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "updateProjectByIdResponse")
    public JAXBElement<UpdateProjectByIdResponse> createUpdateProjectByIdResponse(UpdateProjectByIdResponse value) {
        return new JAXBElement<UpdateProjectByIdResponse>(_UpdateProjectByIdResponse_QNAME, UpdateProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIndex }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "updateProjectByIndex")
    public JAXBElement<UpdateProjectByIndex> createUpdateProjectByIndex(UpdateProjectByIndex value) {
        return new JAXBElement<UpdateProjectByIndex>(_UpdateProjectByIndex_QNAME, UpdateProjectByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProjectByIndexResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.ichaplygina.tsc.ru/", name = "updateProjectByIndexResponse")
    public JAXBElement<UpdateProjectByIndexResponse> createUpdateProjectByIndexResponse(UpdateProjectByIndexResponse value) {
        return new JAXBElement<UpdateProjectByIndexResponse>(_UpdateProjectByIndexResponse_QNAME, UpdateProjectByIndexResponse.class, null, value);
    }

}
