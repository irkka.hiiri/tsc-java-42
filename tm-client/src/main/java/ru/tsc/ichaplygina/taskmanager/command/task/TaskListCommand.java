package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.endpoint.TaskDTO;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_HINT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "list tasks";

    @NotNull
    public final static String DESCRIPTION = "show all tasks";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String sortBy = readLine(SORT_HINT);
        @NotNull final List<TaskDTO> taskList = getTaskEndpoint().getTaskList(getSession(), sortBy);
        if (taskList == null) return;
        System.out.println("Id : Name : Description : Created : Status : Start Date : End Date : User Id");
        int index = 1;
        for (@NotNull final TaskDTO task : taskList) {
            System.out.println(index + DELIMITER +
                    task.getId() + DELIMITER +
                    task.getName() + DELIMITER +
                    task.getDescription() + DELIMITER +
                    task.getCreated() + DELIMITER +
                    task.getStatus() + DELIMITER +
                    task.getDateStart() + DELIMITER +
                    task.getDateFinish() + DELIMITER +
                    task.getUserId());
            index++;
        }
    }

}
