package ru.tsc.ichaplygina.taskmanager.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.endpoint.SessionDTO;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class LoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "login into the system";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String password = readLine(ENTER_PASSWORD);
        @Nullable final SessionDTO currentSession = getSession();
        if (currentSession != null) getSessionEndpoint().closeSession(currentSession);
        final SessionDTO session = getSessionEndpoint().openSession(login, password);
        setSession(session);
    }

}
