package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.endpoint.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected final String ENTER_LOGIN = "Enter login: ";

    @NotNull
    protected final String ENTER_PASSWORD = "Enter password: ";

    @NotNull
    protected final String ENTER_EMAIL = "Enter email: ";

    @NotNull
    protected final String ENTER_ROLE = "Enter user role: (Admin, User; default: User) ";

    @NotNull
    protected final String ENTER_FIRST_NAME = "Enter first name: ";

    @NotNull
    protected final String ENTER_MIDDLE_NAME = "Enter middle name: ";

    @NotNull
    protected final String ENTER_LAST_NAME = "Enter last name: ";

    @NotNull
    protected final String USER_ALREADY_LOCKED = "User already locked. ";

    @NotNull
    protected final String USER_IS_NOT_LOCKED = "User is not locked. ";

    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showUser(@NotNull final UserDTO user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Role: " + user.getRole());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Locked: " + user.isLocked());
    }

}
