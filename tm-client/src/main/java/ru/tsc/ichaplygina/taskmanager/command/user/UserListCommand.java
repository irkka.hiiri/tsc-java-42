package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.endpoint.UserDTO;

import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.DELIMITER;

public final class UserListCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "list users";

    @NotNull
    public static final String DESCRIPTION = "show all users";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final List<UserDTO> userList = getAdminEndpoint().getUsers(getSession());
        System.out.println("Id : Login : Role : E-mail : First Name : Middle Name : Last Name : Locked");
        for (final UserDTO user : userList) {
            System.out.println(user.getId() + DELIMITER + user.getLogin() + DELIMITER + user.getRole() + DELIMITER +
                    user.getEmail() + DELIMITER + user.getFirstName() + DELIMITER + user.getMiddleName() + DELIMITER +
                    user.getLastName() + DELIMITER + user.isLocked());
        }
    }

}
