package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.endpoint.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.endpoint.ProjectEndpoint;
import ru.tsc.ichaplygina.taskmanager.exception.other.EndpointLocatorNotFoundException;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected ProjectEndpoint getProjectEndpoint() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getProjectEndpoint();
    }

    protected void showProject(@NotNull final ProjectDTO project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Created: " + project.getCreated());
        System.out.println("Status: " + project.getStatus());
        System.out.println("Started: " + project.getDateStart());
        System.out.println("Finished: " + project.getDateFinish());
        System.out.println("User Id: " + project.getUserId());
    }

}
