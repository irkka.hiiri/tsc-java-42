package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;

public interface EndpointLocator {

    @NotNull ICommandService getCommandService();

    @NotNull IPropertyService getPropertyService();

    @NotNull AdminEndpoint getAdminEndpoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @Nullable SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

}
