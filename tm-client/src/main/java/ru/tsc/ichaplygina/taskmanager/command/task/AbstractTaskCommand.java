package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.endpoint.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.endpoint.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.exception.other.EndpointLocatorNotFoundException;

import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public static final String TASK_ID_INPUT = "Please enter task id: ";

    @NotNull
    public static final String PROJECT_ID_INPUT = "Please enter project id: ";

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    protected TaskEndpoint getTaskEndpoint() {
        if (!Optional.ofNullable(endpointLocator).isPresent()) throw new EndpointLocatorNotFoundException();
        return endpointLocator.getTaskEndpoint();
    }

    protected void showTask(@NotNull final TaskDTO task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Created: " + task.getCreated());
        System.out.println("Status: " + task.getStatus());
        System.out.println("Started: " + task.getDateStart());
        System.out.println("Finished: " + task.getDateFinish());
        System.out.println("User Id: " + task.getUserId());
    }

}
